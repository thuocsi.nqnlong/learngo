package api

import (
	"fmt"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk"
	"go-sdk-api/model"
	"go.mongodb.org/mongo-driver/bson"
)

func ReadById(req sdk.APIRequest, res sdk.APIResponder) error {
	id := req.GetVar("id")
	return res.Respond(model.UserDB.QueryOne(bson.D{{"id", id}}))
}

func ReadUser(req sdk.APIRequest, res sdk.APIResponder) error {
	fmt.Println(req.GetContentText())
	if req.GetContentText() != "" {

	}
	name := req.GetParam("name")
	//age, _ := strconv.Atoi(req.GetParam("age"))

	return res.Respond(model.UserDB.Query(bson.D{
		{"name", name},
	}, 0, 1, nil))

}

func ReadAllUsers(req sdk.APIRequest, res sdk.APIResponder) error {
	return res.Respond(model.UserDB.QueryAll())
}
