package api

import (
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/common"
	"go-sdk-api/model"
)

func CreateUser(req sdk.APIRequest, res sdk.APIResponder) error {
	var user model.User
	req.GetContent(&user)
	if user.Id == "" {
		return res.Respond(&common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "Can not create empty User",
		})
	}
	return res.Respond(model.UserDB.Create(user))
}

func CreateAllUsers(req sdk.APIRequest, res sdk.APIResponder) error {
	var users []model.User
	req.GetContent(&users)
	for i, u := range users {
		if u.Id == "" || u.Name == "" {
			if len(users) <= 1 {
				users = []model.User{}
			} else {
				users = append(users[:i], users[i+1:]...)
			}
		}
	}
	//var i = []int{1, 2, 3, 4, 5, 6}
	//for index, v := range i {
	//	if v == 2 || v == 4 {
	//		i = append(i[:index], i[index+1:]...)
	//		fmt.Println(i)
	//	}
	//}
	//fmt.Println(i)
	//fmt.Println(reflect.TypeOf(users[0]))
	//fmt.Println(users[1])
	return res.Respond(model.UserDB.CreateMany(users))
}
