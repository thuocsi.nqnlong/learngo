package api

import (
	"fmt"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk"
	"go-sdk-api/model"
	"go.mongodb.org/mongo-driver/bson"
)

func UpdateUser(req sdk.APIRequest, res sdk.APIResponder) error {
	id := req.GetVar("id")
	fmt.Println(id)
	var user model.User
	req.GetContent(&user)
	user.Id = id
	fmt.Println(user)
	return res.Respond(model.UserDB.UpdateOne(model.User{
		Id: id,
	}, user))
}

func UpdateAllUsers(req sdk.APIRequest, res sdk.APIResponder) error {
	var users []model.User
	req.GetContent(&users)
	return res.Respond(model.UserDB.UpdateMany(model.User{
		Name: "Long",
	}, bson.M{"name": "Ngoc Long"}))
}
