package api

import (
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/client"
)

func TestSDK(req sdk.APIRequest, res sdk.APIResponder) error {

	var apiclient client.APIClient
	var config = client.APIClientConfiguration{
		Address:  "127.0.0.1:4000",
		Protocol: "HTTP",
	}

	apiclient = client.NewAPIClient(&config)
	return res.Respond(apiclient.MakeRequest(req))
}
