package api

import (
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk"
	"go-sdk-api/model"
)

func DeleteUser(req sdk.APIRequest, res sdk.APIResponder) error {
	id := req.GetVar("id")
	return res.Respond(model.UserDB.Delete(model.User{
		Id: id,
	}))
}

func DeleteAllUsers(req sdk.APIRequest, res sdk.APIResponder) error {
	return res.Respond(model.UserDB.Delete(model.User{}))
}
