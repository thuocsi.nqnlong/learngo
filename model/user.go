package model

import (
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/db"
	"go.mongodb.org/mongo-driver/mongo"
)

type User struct {
	Id     string `json:"id,omitempty" bson:"id,omitempty"`
	Name   string `json:"name,omitempty" bson:"name,omitempty"`
	Age    int    `json:"age,omitempty" bson:"age,omitempty"`
	Gender string `json:"gender,omitempty" bson:"gender,omitempty"`
	Dob    string `json:"dob,omitempty" bson:"dob,omitempty"`
}

var UserDB = db.Instance{
	ColName:        "Users",
	TemplateObject: &User{},
}

func InitUserDB(database *mongo.Database) {
	UserDB.ApplyDatabase(database)
}
