package job

import (
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/common"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

// ExecutionSelector ...
type ExecutionSelector struct {
	name            string
	channels        []*ExecutionChannel
	jobDB           *db.Instance
	version         string
	acceptableTopic []string
	config          *ExecutorConfiguration
}

func (es *ExecutionSelector) pickFreeChannel(start int, limit int) int {
	var quota = limit
	var i = start
	for quota > 0 {
		if !es.channels[i].processing {
			es.channels[i].processing = true
			return i
		}
		i = (i + 1) % limit
		quota--
	}
	return -1
}

func (es *ExecutionSelector) start() {

	var counter = 0
	for true {

		if es.config.ParallelTopicProcessing {
			var topics []interface{}
			topicResult := es.jobDB.Distinct(nil, "topic")

			if topicResult.Status == common.APIStatus.Ok {
				topics = topicResult.Data.([]interface{})
				if len(topics) > 0 {

					for _, topic := range topics {

						// scan to check if topic exist in acceptableTopic
						var found = es.acceptableTopic == nil || len(es.acceptableTopic) == 0
						if topic == nil {
							topic = "default"
							found = true
						}
						if !found {
							for _, topicName := range es.acceptableTopic {
								if topicName == topic {
									found = true
									break
								}
							}
						}

						// if topic available in acceptableTopics
						if found {
							if es.config.SortedItem {
								es.scanTopic(topic.(string))
							} else {
								es.doSelect(topic.(string), "")
							}
						}
					}
				} else {
					time.Sleep(time.Duration(es.config.SelectorDelayMS) * time.Millisecond)
				}
			} else {
				time.Sleep(time.Duration(es.config.SelectorDelayMS) * time.Millisecond)
			}
		} else {
			if es.config.SortedItem {
				es.scanTopic("")
			} else {
				es.doSelect("", "")
			}
		}

		// check & clean old item
		counter++
		if counter > 500 {

			// clean old item with different version
			oldTime := time.Now().Add(-(time.Duration(es.config.OldVersionTimeoutS) * time.Second))
			es.jobDB.UpdateMany(&bson.M{
				"process_by": bson.M{
					"$ne": "NONE",
				},
				"consumer_version": bson.M{
					"$ne": es.version,
				},
				"last_updated_time": bson.M{
					"$lt": oldTime,
				},
			}, &bson.M{
				"process_by":       "NONE",
				"consumer_version": "NONE",
			})

			// all version
			oldTime = oldTime.Add(-(time.Duration(es.config.CurVersionTimeoutS) * time.Second))
			es.jobDB.UpdateMany(&bson.M{
				"process_by": bson.M{
					"$ne": "NONE",
				},
				"last_updated_time": bson.M{
					"$lt": oldTime,
				},
			}, &bson.M{
				"process_by":       "NONE",
				"consumer_version": "NONE",
			})

			counter = 0
		}

	}
}

func (es *ExecutionSelector) scanTopic(topic string) {
	var query = bson.M{
		"process_by": "NONE",
	}
	if topic != "" {
		query["topic"] = topic
	} else if es.acceptableTopic != nil && len(es.acceptableTopic) > 0 {
		query["topic"] = bson.M{
			"$in": es.acceptableTopic,
		}
	}

	var keys []interface{}
	keysResult := es.jobDB.Distinct(&query, "sorted_key")
	if keysResult.Status == common.APIStatus.Ok {
		keys = keysResult.Data.([]interface{})
		if len(keys) > 0 {
			for _, key := range keys {
				if key == nil {
					es.doSelect(topic, "default")
				} else {
					es.doSelect(topic, key.(string))
				}
			}
		} else {
			time.Sleep(10 * time.Millisecond)
		}
	} else {
		time.Sleep(10 * time.Millisecond)
	}
}

// doSelect select item with topic name & sorted key value
func (es *ExecutionSelector) doSelect(topic string, sortedKey string) {
	var channelNum = len(es.channels)

	// pick free channel
	picked := -1
	for picked < 0 {
		picked = es.pickFreeChannel(0, channelNum)
		if picked < 0 {
			// if all channels are busy
			time.Sleep(10 * time.Millisecond)
		}
	}

	// setup query
	var query *bson.M
	if !es.config.ParallelTopicProcessing && es.acceptableTopic != nil && len(es.acceptableTopic) > 0 {
		query = &bson.M{
			"process_by": "NONE",
			"topic": &bson.M{
				"$in": es.acceptableTopic,
			},
		}
	} else {
		if topic == "" {
			topic = "default"
		}
		query = &bson.M{
			"process_by": "NONE",
			"topic":      topic,
		}
	}

	if es.config.WaitForReadyTime {
		(*query)["ready_time"] = &bson.M{
			"$lt": time.Now(),
		}
	}

	// pick an item from DB
	var resp *common.APIResponse
	if es.config.SortedItem {
		(*query)["sorted_key"] = sortedKey

		resp = es.jobDB.Query(query, 0, 1, &bson.M{"sort_index":1})
		if resp.Status == common.APIStatus.Ok {
			item := resp.Data.([]*JobItem)[0]
			if item.ProcessBy == "NONE" {
				resp = es.jobDB.UpdateOne(query, &bson.M{
					"process_by":       es.name + " - " + es.channels[picked].name,
					"consumer_version": es.version,
				}, &options.FindOneAndUpdateOptions{
					Sort: bson.M{"sort_index":1},
				})
				if resp.Status == common.APIStatus.Ok {
					item = resp.Data.([]*JobItem)[0]
					if !es.validateItemOrder(item) {
						resp.Status = common.APIStatus.Existed
						es.jobDB.UpdateOne(&JobItem{
							ID: item.ID,
						}, &JobItem{
							ProcessBy: "NONE",
						}, &options.FindOneAndUpdateOptions{
							Sort: bson.M{"sort_index":1},
						})
					}
				}
			} else {
				resp.Status = common.APIStatus.Existed
			}
		}
	} else {
		after := options.After
		resp = es.jobDB.UpdateOne(query, &bson.M{
			"process_by":       es.name + " - " + es.channels[picked].name,
			"consumer_version": es.version,
		}, &options.FindOneAndUpdateOptions{
			Sort: bson.M{"sort_index":1},
			ReturnDocument: &after,
		})
	}

	if resp.Status == common.APIStatus.Ok {
		item := resp.Data.([]*JobItem)[0]
		es.channels[picked].putItem(item)
		time.Sleep(10 * time.Millisecond)
	} else {
		// if no item found
		es.channels[picked].processing = false
		if !es.config.ParallelTopicProcessing && !es.config.SortedItem { // sleep shorter if parallel processing
			time.Sleep(time.Duration(es.config.SelectorDelayMS) * time.Millisecond)
		} else {
			time.Sleep(100 * time.Millisecond)
		}
	}
}

func (es *ExecutionSelector) validateItemOrder(item *JobItem) bool {
	if item.SortedKey == "" {
		return true
	}

	itemRs := es.jobDB.Query(&JobItem{
		SortedKey: item.SortedKey,
	}, 0, 1,  &bson.M{"sort_index":1})

	if itemRs.Status == common.APIStatus.Ok {
		firstItem := itemRs.Data.([]*JobItem)[0]
		if firstItem.SortIndex < item.SortIndex {
			return false
		}
	}
	return true
}
